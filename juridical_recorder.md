---
layout: page
title: Juridical Recorder für ETCS und DSTW
permalink: /juridical_recorder/
hide_from_menu: true
---

<p><video width="100%" controls preload="auto" src="https://railchain-media.beta.de.com/stream/railchain-adv-2020-hd-v2.mp4"></video></p>

Im Bereich ETCS/ERTMS existiert eine Spezifikation im Subset-027 zum Juridical Recording im Sinne der Unfalldatenspeicherung.
Diese wird von den Herstellern durch eine Juridical Recording Unit (JRU) bereitgestellt, die neben den Ereignissen des ETCS-Hauptrechners (European Vital Computer / EVC) auch die Signale der Class-B-Systeme speichert.
Die Fälschungssicherheit des Datenspeichers steht hierbei im Vordergrund.
Neben allen externen Ereignissen wie erkannten Eurobalisen und internen Ereignissen, wie z. B. betätigte Bremsen, wird mindestens alle fünf Sekunden eine allgemeine Zustandsinformation gespeichert (mit Zeitstempel, Zugposition, Geschwindigkeit, Betriebssystemversion, ETCS-Level und -Modus).

<p><video width="100%" controls preload="auto" src="https://railchain-media.beta.de.com/stream/railchain-testfahrt-2021-v2.mp4"></video></p>

Der dritte Use Case befasst sich mit der Überführung einer heutigen dedizierten, physischen Juridical Recording Unit auf eine Blockchain-basierte Lösung mit Echtzeitanforderungen (<1 Sekunde Block-Zyklus-Zeit) zur Erprobung in einem in einem geeigneten Testumfeld.
Neben der Fälschungssicherheit des Datenspeichers wird hier auch auf Interoperabilitätsanforderungen zwischen mehreren Herstellern geachtet (z. B. standardisierte Kommunikationsprotokolle).
In diesem Schritt ist zusätzlich eine über die Blockchain- bzw. Distributed Ledger-Technologie abgesicherte Kommunikationsverschlüsselung für ETCS, sowie das Themenfeld der Softwareaktualisierungen Over The Air über Blockchain zu prüfen.
